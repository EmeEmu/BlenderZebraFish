import bpy
import numpy as np

import LightSheet as LS
import Neurons as NS
from utils import activity_modulation, traj_from_keys, generate_activity, overlapYN, overlaping, single_Zoverlap


def gcamp(startT=0, stopT=500, limit_animation=True):
    """Explain how calcium indicators work."""

    if limit_animation:  # limit animation to stat and stop frames
        bpy.context.scene.frame_start = startT
        bpy.context.scene.frame_end = stopT

    # time
    T = np.arange(startT, stopT + 1)  # frame numbers
    N = len(T)
    t = np.linspace(0, 1, N)  # animation fraction

    # camera
    bpy.ops.object.camera_add(
        location=(62, -28, 20), rotation=(1.2391, 0, 1.0559), scale=(1, 1, 1)
    )

    # world
    bpy.context.scene.world = bpy.data.worlds["World"]

    # gaussian beam ___________________________________________________________
    LS.gaussian_beam()  # create the beam
    gb_y_keys = np.array(  # horizontal scaning positions
        [
            [0, -29],
            [0.096, 0],
            [0.288, 0],
            [0.336, 15],
            [0.48, 15],
            [0.528, 0],
            [1, 0],
        ]
    )
    gb_y = traj_from_keys(  # horizontal positions at each frame
        gb_y_keys[:, 1],
        gb_y_keys[:, 0],
        t,
    )
    for i in range(N):
        bpy.context.scene.frame_set(T[i])
        bpy.data.objects["gaussian_beam"].location = (0, gb_y[i], 0)
        bpy.data.objects["gaussian_beam"].keyframe_insert(data_path="location")
    # gaussian beam ___________________________________________________________

    # filter ___________________________________________________________
    LS.filter()  # create filter
    bpy.data.objects["filter_1"].rotation_euler = (1.571, 0.0, 1.047)
    bpy.data.objects["filter_2"].rotation_euler = (1.571, 0.0, 1.047)

    bpy.data.objects["filter_1"].location = (32.4, -11.4, 29)
    bpy.data.objects["filter_2"].location = (32.4, -11.4, 29)
    bpy.context.scene.frame_set(startT)  # initial position
    bpy.data.objects["filter_1"].keyframe_insert(data_path="location")
    bpy.data.objects["filter_2"].keyframe_insert(data_path="location")

    bpy.context.scene.frame_set(int(0.57 * (stopT - startT)))
    bpy.data.objects["filter_1"].keyframe_insert(data_path="location")
    bpy.data.objects["filter_2"].keyframe_insert(data_path="location")

    bpy.context.scene.frame_set(int(0.72 * (stopT - startT)))
    bpy.data.objects["filter_1"].location = (32.4, -11.4, 8.1)
    bpy.data.objects["filter_2"].location = (32.4, -11.4, 8.1)
    bpy.data.objects["filter_1"].keyframe_insert(data_path="location")
    bpy.data.objects["filter_2"].keyframe_insert(data_path="location")
    # filter ___________________________________________________________

    # neuron ___________________________________________________________
    NS.neurons(N=1)  # create neuron
    bpy.data.objects["neuron_0"].location = (0, 0, 0)

    ill = activity_modulation([0, 0, 0], 4, 0, Y=gb_y)  # illumination of neuron

    # get neuron activity info
    dff = np.load("data/dff.npy")
    dff = dff[37098]
    t0 = 1787
    dff = dff[t0 : t0 + N]
    dff[np.where(dff < 0)] = 0
    dff = dff * 100 / np.max(dff) * ill
    dff = dff.astype(int)

    for i in range(N):
        bpy.context.scene.frame_set(T[i])
        bpy.data.objects["neuron_0"].pass_index = dff[i]
        bpy.data.objects["neuron_0"].keyframe_insert(data_path="pass_index")
    # neuron ___________________________________________________________


def lightsheet(startT=0, stopT=500, limit_animation=True):
    """Explain how the lightsheet is created and used."""

    if limit_animation:  # limit animation to stat and stop frames
        bpy.context.scene.frame_start = startT
        bpy.context.scene.frame_end = stopT

    # time
    T = np.arange(startT, stopT + 1)  # frame numbers
    N = len(T)
    t = np.linspace(0, 1, N)  # animation fraction

    # camera
    bpy.ops.object.camera_add(
        location=(0, 0, 343), rotation=(0, 0, np.pi / 2), scale=(1, 1, 1)
    )

    # world
    bpy.context.scene.world = bpy.data.worlds["World_alpha"]

    # gaussian beam ___________________________________________________________
    LS.gaussian_beam(alpha=True)  # create the beam
    # bpy.context.scene.frame_set(T[0])
    # bpy.data.objects["gaussian_beam"].hide_render = False
    # bpy.data.objects['gaussian_beam'].keyframe_insert(data_path="hide_render")
    # a, A = 1.0e3, 100
    # end = int(N/2)
    # gb_y = A * np.sin(a * t[: end] ** 4)
    A = 100
    start = 0
    nb_periods = 14
    periods = 1 / np.linspace(0, 1, nb_periods) + 10
    periods[0] = 0
    periods = periods / np.sum(periods)

    keys_t = np.cumsum(periods)  # np.arange(0,1,T)
    keys_t = np.append(keys_t, 1)
    keys_x = np.tile(np.array([-A, +A]), int(len(keys_t) / 2))
    keys_x[0] = start
    keys_x = np.append(keys_x, keys_x[-1])

    gb_y = traj_from_keys(keys_x, keys_t, t)
    for i in range(N):
        bpy.context.scene.frame_set(T[i])
        bpy.data.objects["gaussian_beam"].location = (0, gb_y[i], 0)
        bpy.data.objects["gaussian_beam"].keyframe_insert(data_path="location")
        bpy.data.objects["gaussian_beam"].pass_index = int(t[i] * 100)
        bpy.data.objects["gaussian_beam"].keyframe_insert(data_path="pass_index")
    # bpy.context.scene.frame_set(T[end])
    # bpy.data.objects["gaussian_beam"].hide_render = True
    # bpy.data.objects['gaussian_beam'].keyframe_insert(data_path="hide_render")
    # gaussian beam ___________________________________________________________

    # light sheet _____________________________________________________________
    LS.light_sheet(alpha=True)  # create the light sheet
    for i in range(N):
        bpy.context.scene.frame_set(T[i])
        bpy.data.objects["light_sheet"].pass_index = int((1 - t[i]) * 100)
        bpy.data.objects["light_sheet"].keyframe_insert(data_path="pass_index")
    # bpy.context.scene.frame_set(T[0])
    # bpy.data.objects["light_sheet"].hide_render = True
    # bpy.data.objects['light_sheet'].keyframe_insert(data_path="hide_render")
    # bpy.context.scene.frame_set(T[end])
    # bpy.data.objects["light_sheet"].hide_render = False
    # bpy.data.objects['light_sheet'].keyframe_insert(data_path="hide_render")

    # light sheet _____________________________________________________________


def multi_lightsheet(startT=0, stopT=500, limit_animation=True, nNeurons=10, span=50):
    """Explain lightsheet microscopy with multiple neurons."""

    if limit_animation:  # limit animation to stat and stop frames
        bpy.context.scene.frame_start = startT
        bpy.context.scene.frame_end = stopT

    # time
    T = np.arange(startT, stopT + 1)  # frame numbers
    N = len(T)
    t = np.linspace(0, 1, N)  # animation fraction

    """
    # camera
    bpy.ops.object.camera_add(
        location=(62, -28, 20), rotation=(1.2391, 0, 1.0559), scale=(1, 1, 1)
    )
    """

    # world
    bpy.context.scene.world = bpy.data.worlds["World"]

    # LightSheet _________________________________________________
    LS.light_sheet(alpha=False)  # create the light sheet
    ls_z_keys = np.array(
            [
                [0, 0],
                [0.25, 0],
                [0.30, span*0.5],
                [0.50, span*0.5],
                [0.55, span*0.75],
                [0.60, span*0.75],
                [0.65, span],
                [0.70, span],
                [0.75, span*0.75],
                [0.80, span*0.75],
                [0.85, span*0.5],
                [0.90, span*0.5],
                [1, 0],
            ])
    ls_z = traj_from_keys(ls_z_keys[:,1], ls_z_keys[:,0], t)
    for i in range(N):
        bpy.context.scene.frame_set(T[i])
        bpy.data.objects["light_sheet"].location = (0, 0, ls_z[i])
        bpy.data.objects["light_sheet"].keyframe_insert(data_path="location")

    # neuron ___________________________________________________________
    NS.neurons(N=nNeurons)  # create neuron
    positions = np.random.uniform(low=-span, high=+span, size=(nNeurons, 3))
    dffs = generate_activity(t, n=nNeurons, p=0.1, tau=0.01)
    for i in range(nNeurons):
        bpy.data.objects[f"neuron_{i}"].location = tuple(positions[i])

    """
    ill = activity_modulation([0, 0, 0], 4, 0, Y=gb_y)  # illumination of neuron

    # get neuron activity info
    dff = np.load("data/dff.npy")
    dff = dff[37098]
    t0 = 1787
    dff = dff[t0 : t0 + N]
    dff[np.where(dff < 0)] = 0
    dff = dff * 100 / np.max(dff) * ill
    dff = dff.astype(int)
    """

    lightsheet = bpy.data.objects["light_sheet"]
    for i in range(N):
        for neur in range(nNeurons):
            bpy.context.scene.frame_set(T[i])
            neuron = bpy.data.objects[f"neuron_{neur}"]
            zdist = abs(neuron.location[2] - lightsheet.location[2])
            # overlap = overlapYN(lightsheet,neuron)
            # overlap = overlaping(zdist, R=1.5, r=4)
            overlap = single_Zoverlap(neuron.location, lightsheet.location)
            neuron.pass_index = int(dffs[neur, i]*overlap)
            neuron.keyframe_insert(data_path="pass_index")
    # neuron ___________________________________________________________


