"""Methods to build and animate light sheets and optic-related objects."""
import bpy
import numpy as np


def create_collection():
    # create a new collection for Light Sheet related objects or use existing
    collections = [col[0] for col in bpy.data.collections.items()]
    if "Light_Sheet_collection" not in collections:
        ls_col = bpy.data.collections.new("Light_Sheet_collection")
        bpy.context.scene.collection.children.link(ls_col)
    else:
        ls_col = bpy.data.collections["Light_Sheet_collection"]
    return ls_col


# parameters
lam = 0.488
wo = 2.04
n = 1.33
min_x, max_x = -100, 100
min_y, max_y = -100, 100
min_z, max_z = -100, 100

zR = (np.pi * wo * wo * n) / lam
w = f"{wo} * sqrt( 1 + ( u/{zR} )**2 )"


def ww(r):
    """Gaussian beam profile."""
    return wo * np.sqrt(1 + (r / zR) ** 2)


def light_sheet(alpha=False):
    """Create a gaussian light_sheet."""
    ls_col = create_collection()

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="u",
        y_eq="v",
        z_eq=f"{w}",
        range_u_min=min_x,
        range_u_max=max_x,
        range_u_step=60,
        wrap_u=False,
        range_v_min=min_y,
        range_v_max=max_y,
        range_v_step=60,
        wrap_v=False,
        show_wire=False,
        edit_mode=False,
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="u",
        y_eq="v",
        z_eq=f"-{w}",
        range_u_min=min_x,
        range_u_max=max_x,
        range_u_step=60,
        wrap_u=False,
        range_v_min=min_y,
        range_v_max=max_y,
        range_v_step=60,
        wrap_v=False,
        show_wire=False,
        edit_mode=False,
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq=f"{min_x}",
        y_eq="v",
        z_eq="u",
        range_u_min=-ww(min_x),
        range_u_max=ww(min_x),
        range_u_step=60,
        wrap_u=False,
        range_v_min=min_y,
        range_v_max=max_y,
        range_v_step=60,
        wrap_v=False,
        show_wire=False,
        edit_mode=False,
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq=f"{max_x}",
        y_eq="v",
        z_eq="u",
        range_u_min=-ww(max_x),
        range_u_max=ww(max_x),
        range_u_step=60,
        wrap_u=False,
        range_v_min=min_y,
        range_v_max=max_y,
        range_v_step=60,
        wrap_v=False,
        show_wire=False,
        edit_mode=False,
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="u",
        y_eq=f"({w})*cos(v)",
        z_eq=f"({w})*sin(v)",
        range_u_min=min_x,
        range_u_max=max_x,
        range_u_step=60,
        wrap_u=False,
        range_v_min=-np.pi / 2,
        range_v_max=np.pi / 2,
        range_v_step=128,
        wrap_v=False,
        show_wire=False,
        edit_mode=False,
    )
    bpy.ops.transform.translate(
        value=(0, max_y, 0), orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1))
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="u",
        y_eq=f"(-{w})*cos(v)",
        z_eq=f"({w})*sin(v)",
        range_u_min=min_x,
        range_u_max=max_x,
        range_u_step=60,
        wrap_u=False,
        range_v_min=-np.pi / 2,
        range_v_max=np.pi / 2,
        range_v_step=128,
        wrap_v=False,
        show_wire=False,
        edit_mode=False,
    )
    bpy.ops.transform.translate(
        value=(0, min_y, 0), orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1))
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="0",
        y_eq="u*cos(v)",
        z_eq="u*sin(v)",
        range_u_min=0,
        range_u_max=ww(max_x),
        range_u_step=60,
        wrap_u=False,
        range_v_min=-0.5 * np.pi,
        range_v_max=0.6 * np.pi,
        range_v_step=128,
        wrap_v=True,
        show_wire=False,
        edit_mode=False,
    )
    bpy.ops.transform.translate(
        value=(min_x, max_y, 0),
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="0",
        y_eq="u*cos(v)",
        z_eq="u*sin(v)",
        range_u_min=0,
        range_u_max=ww(max_x),
        range_u_step=60,
        wrap_u=False,
        range_v_min=-0.5 * np.pi,
        range_v_max=0.6 * np.pi,
        range_v_step=128,
        wrap_v=True,
        show_wire=False,
        edit_mode=False,
    )
    bpy.ops.transform.translate(
        value=(max_x, max_y, 0),
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="0",
        y_eq="-u*cos(v)",
        z_eq="u*sin(v)",
        range_u_min=0,
        range_u_max=ww(max_x),
        range_u_step=60,
        wrap_u=False,
        range_v_min=-0.5 * np.pi,
        range_v_max=0.6 * np.pi,
        range_v_step=128,
        wrap_v=True,
        show_wire=False,
        edit_mode=False,
    )
    bpy.ops.transform.translate(
        value=(min_x, min_y, 0),
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="0",
        y_eq="-u*cos(v)",
        z_eq="u*sin(v)",
        range_u_min=0,
        range_u_max=ww(max_x),
        range_u_step=60,
        wrap_u=False,
        range_v_min=-0.5 * np.pi,
        range_v_max=0.6 * np.pi,
        range_v_step=128,
        wrap_v=True,
        show_wire=False,
        edit_mode=False,
    )
    bpy.ops.transform.translate(
        value=(max_x, min_y, 0),
        orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
    )
    obj = bpy.context.active_object  # get current object
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    # join all lightsheet parts
    bpy.ops.object.select_all(action="DESELECT")
    OBJS = [m for m in bpy.data.collections["Light_Sheet_collection"].all_objects]
    for obj in OBJS[::-1]:
        # [::-1] so the last object selected is the fist created
        # -> center of selection at (0,0,0)
        if obj.name[:12] == "XYZ Function":
            obj.select_set(state=True)
            bpy.context.view_layer.objects.active = obj
    bpy.ops.object.join()
    obj = bpy.context.active_object  # get current object
    obj.name = "light_sheet"

    bpy.ops.object.shade_smooth()

    if alpha:
        obj.data.materials.append(bpy.data.materials["laser_alpha"])  # set material
    else:
        obj.data.materials.append(bpy.data.materials["laser"])  # set material


def gaussian_beam(alpha=False):
    """Create a gaussian laser beam."""
    ls_col = create_collection()

    bpy.ops.mesh.primitive_circle_add(
        vertices=128,
        radius=ww(max_x),
        fill_type="NGON",
        rotation=(0, np.pi / 2, 0),
        location=(max_x, 0, 0),
    )
    obj = bpy.context.active_object  # get current object
    obj.name = "gaussian_beam_1"
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_circle_add(
        vertices=128,
        radius=ww(min_x),
        fill_type="NGON",
        rotation=(0, np.pi / 2, 0),
        location=(min_x, 0, 0),
    )
    obj = bpy.context.active_object  # get current object
    obj.name = "gaussian_beam_2"
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    bpy.ops.mesh.primitive_xyz_function_surface(
        x_eq="u",
        y_eq=f"({w})*cos(v)",
        z_eq=f"({w})*sin(v)",
        range_u_min=min_x,
        range_u_max=100,
        range_u_step=60,
        wrap_u=False,
        range_v_min=0,
        range_v_max=2 * np.pi,
        range_v_step=128,
        wrap_v=True,
        show_wire=False,
        edit_mode=False,
    )

    obj = bpy.context.active_object  # get current object
    obj.name = "gaussian_beam_3"
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection

    # join all gaussian beam parts
    bpy.ops.object.select_all(action="DESELECT")
    OBJS = [m for m in bpy.data.collections["Light_Sheet_collection"].all_objects]
    for obj in OBJS:
        if obj.name[:13] == "gaussian_beam":
            obj.select_set(state=True)
            bpy.context.view_layer.objects.active = obj
    bpy.ops.object.join()
    obj = bpy.context.active_object  # get current object
    obj.name = "gaussian_beam"

    bpy.ops.object.shade_smooth()

    if alpha:
        obj.data.materials.append(bpy.data.materials["laser_alpha"])  # set material
    else:
        obj.data.materials.append(bpy.data.materials["laser"])  # set material


def filter():
    """Create a filter for gcamp wavelength."""
    ls_col = create_collection()

    bpy.ops.mesh.primitive_cylinder_add(radius=10, depth=1)
    obj = bpy.context.active_object  # get current object
    obj.name = "filter_1"
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection
    bpy.ops.object.shade_smooth()
    obj.data.materials.append(bpy.data.materials["filter"])  # set material

    bpy.ops.mesh.primitive_torus_add(minor_radius=1, major_radius=10)
    obj = bpy.context.active_object  # get current object
    obj.name = "filter_2"
    bpy.ops.collection.objects_remove_all()  # remove obj from other colections
    ls_col.objects.link(obj)  # assign obj to collection
    bpy.ops.object.shade_smooth()
    obj.data.materials.append(bpy.data.materials["filter_outer"])  # set material

    # # join all gaussian beam parts
    # bpy.ops.object.select_all(action="DESELECT")
    # OBJS = [m for m in bpy.data.collections["Light_Sheet_collection"].all_objects]
    # for obj in OBJS:
    #     if obj.name[:6] == "filter":
    #         obj.select_set(state=True)
    #         bpy.context.view_layer.objects.active = obj
    # bpy.ops.object.join()
    # obj = bpy.context.active_object  # get current object
    # obj.name = "filter"

    # bpy.ops.object.shade_smooth()

    # obj.data.materials.append(bpy.data.materials["laser"])  # set material
