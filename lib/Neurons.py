"""Methods to build and animate light sheets and optic-related objects."""
import bpy
import numpy as np


def create_collection():
    """Create a new collection for Light Sheet related objects."""
    collections = [col[0] for col in bpy.data.collections.items()]
    if "Neuron_collection" not in collections:
        neuron_col = bpy.data.collections.new("Neuron_collection")
        bpy.context.scene.collection.children.link(neuron_col)
    else:
        neuron_col = bpy.data.collections["Neuron_collection"]
    return neuron_col


def neurons(N=10, radius=4, scale=1, initial_activity=50, origin="random"):
    """Build neurons."""
    neuron_col = create_collection()

    if origin == "random":
        coords = np.random.uniform(-10, 10, size=(N, 3))
    elif origin == "data":
        coords = np.load("/home/mkk/Documents/Blender_ZF/data/coords.npy")
        print(np.min(coords), np.max(coords))
        inds = np.arange(len(coords))
        np.random.shuffle(inds)
        inds = inds[:N]
        coords = coords[inds]
    else:
        raise TypeError(f"unknown origin '{origin}'.")

    coords = coords * scale

    for i in range(coords.shape[0]):
        bpy.ops.mesh.primitive_uv_sphere_add(
            radius=radius,
            enter_editmode=False,
            align="WORLD",
            location=coords[i],
            scale=(1, 1, 1),
        )
        bpy.ops.object.shade_smooth()

        obj = bpy.context.active_object  # get current object
        obj.name = f"neuron_{i}"
        bpy.ops.collection.objects_remove_all()  # remove obj from other collections
        neuron_col.objects.link(obj)  # assign obj to collection

        obj.data.materials.append(bpy.data.materials["neuron"])  # set material
        obj.pass_index = initial_activity  # set neuron activity level [0->100]


def animate(T, method="random"):
    """Annimate all neurons in Neuron Collection."""
    neuron_col = create_collection()
    neurons = neuron_col.objects.items()
    nb = len(neurons)

    if method == "random":
        activities = np.random.randint(0, 100, size=(nb, len(T)))
    else:
        raise NotImplementedError("only random was implemented for now.")

    for j,t in enumerate(T):
        bpy.context.scene.frame_set(t)
        for i, neuron in enumerate(neurons):
            print(neuron)
            neuron[1].pass_index = activities[i,j]
            neuron[1].keyframe_insert(data_path="pass_index")
