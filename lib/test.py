import matplotlib.pyplot as plt
import numpy as np

# from utils import traj_from_keys


def easeInOutQuad(xi, xf, t):
    """Ease trajectory between xi and xf during times t."""
    t = t - t[0]
    T = t[-1] - t[0]
    x = np.where(
        t < T / 2,
        0.5 * (2 * t / T) ** 2,
        -0.5 * ((2 * t / T - 1) * (2 * t / T - 3) - 1),
    )
    return x * (xf - xi) + xi


def traj_from_keys(Xs, Ts, t):
    """Build trajectory from keyframes.

    Xs : <1d, 2d, or 3d array> locations at key frames.
    Ts : <1d array of floats 0->1 > key frames.
    t : <1d array of floats 0->1 > all frames.
    """
    x = np.empty_like(t)
    for i in range(len(Ts) - 1):  # for each key frame interval
        inds = np.arange(  # indices of time steps for that interval
            np.where(t >= Ts[i])[0][0],
            np.where(t >= Ts[i + 1])[0][0] + 1,
        )
        ts = t[inds]
        # print(f"{Ts[i]}->{Ts[i+1]} : {ts}")
        x[inds] = easeInOutQuad(Xs[i], Xs[i + 1], ts)

    return x

t = np.linspace(0, 1, 250)

# keys = np.array(
#     [
#         [0, 0],
#         [0.1, 0],
#         [0.4, -5],
#         [0.5, -5],
#         [0.8, 10],
#         [1, 10],
#     ]
# )

# x = traj_from_keys(keys[:, 1], keys[:, 0], t)

# plt.plot(t, x)
# plt.scatter(keys[:, 0], keys[:, 1], color="r")
# plt.show()

# N = 500
# a = 5.0e2
# t = np.linspace(0, 1, N)  # animation fraction
# x = np.sin(a * t ** 4)

# plt.plot(t, x)
# plt.show()

A = 50
start = 0
N = 14
periods = 1/np.linspace(0,1,N)+10
periods[0] = 0
periods = periods/np.sum(periods)

keys_t = np.cumsum(periods)#np.arange(0,1,T)
keys_t = np.append(keys_t,1)
keys_x = np.tile(np.array([-A,+A]),int(len(keys_t)/2))
keys_x[0] = start
keys_x = np.append(keys_x,keys_x[-1])

x = traj_from_keys(keys_x,keys_t,t)

plt.plot(t,x)
plt.scatter(t,x)
plt.scatter(keys_t,keys_x,color="r")
plt.show()
