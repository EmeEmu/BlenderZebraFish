import numpy as np

from LightSheet import ww
from mathutils.bvhtree import BVHTree

def overlapYN(obj1, obj2):
    """Return true if obj1 and obj2 overlap.

    from https://blender.stackexchange.com/a/150047
    """
    mat1 = obj1.matrix_world
    mat2 = obj2.matrix_world

    # Get the geometry in world coordinates
    vert1 = [mat1 @ v.co for v in obj1.data.vertices] 
    poly1 = [p.vertices for p in obj1.data.polygons]

    vert2 = [mat2 @ v.co for v in obj2.data.vertices] 
    poly2 = [p.vertices for p in obj2.data.polygons]

    # Create the BVH trees
    bvh1 = BVHTree.FromPolygons( vert1, poly1 )
    bvh2 = BVHTree.FromPolygons( vert2, poly2 )

    # Test if overlap
    if bvh1.overlap( bvh2 ):
        return 1
    else:
        return 0

def single_Zoverlap(location_neuron, location_lightsheet, rneuron=4):
    zdist = abs(location_neuron[2] - location_lightsheet[2])
    R = ww(location_neuron[0])  # radius of beam at neuron position
    if zdist > rneuron+R:
        return 0
    else:
        min_area = np.pi * min(R, rneuron) ** 2
        r2, R2, d2 = rneuron ** 2, R ** 2, zdist ** 2
        alpha = np.arccos((d2 + r2 - R2) / (2 * zdist * rneuron))
        beta = np.arccos((d2 + R2 - r2) / (2 * zdist * R))
        gamma = (
            r2 * alpha + R2 * beta - 0.5 * (r2 * np.sin(2 * alpha) + R2 * np.sin(2 * beta))
        )
        overlap = gamma / min_area
        print(overlap)
        if np.isnan(overlap) :
            return 1
        else:
            return overlap



def overlaping(d, R=1.5, r=1):
    """Return the fraction of averlaping between 2 circles.

    Radius of circles : <R> and <r>.
    distance between circle centers : <d>.
    """
    if type(d) in [float, int]:  # single evaluation
        d = np.array([d])

    A = np.empty_like(d)

    inds = np.arange(len(d))
    inds_full = np.where(d <= abs(R - r))[0]
    inds_empt = np.where(d >= r + R)[0]
    inds_other = inds[~np.in1d(inds, np.r_[inds_full, inds_empt])]

    A[inds_full] = 1
    A[inds_empt] = 0

    d = d[inds_other]
    min_area = np.pi * min(R, r) ** 2
    r2, R2, d2 = r ** 2, R ** 2, d ** 2
    alpha = np.arccos((d2 + r2 - R2) / (2 * d * r))
    beta = np.arccos((d2 + R2 - r2) / (2 * d * R))
    gamma = (
        r2 * alpha + R2 * beta - 0.5 * (r2 * np.sin(2 * alpha) + R2 * np.sin(2 * beta))
    )
    A[inds_other] = gamma / min_area

    return A


def activity_modulation(xyz, r, Z, Y=None):
    """Compute neuron activity modulation ( 0 -> 1 ).

    xyz : <3D tuple or array> position of neuron
    r : <float> radius of neuron
    Z : <array> layer of beam
    Y : <array or None> scan of beam
        if None : light sheet
    """
    x, y, z = xyz[0], xyz[1], xyz[2]
    R = ww(x)  # radius of beam at neuron position

    if Y is None:  # if dealing with a light sheet
        d = np.abs(Z - z)
    else:  # if dealing with a gaussian beam
        d = np.sqrt((y - Y) ** 2 + (z - Z) ** 2)

    return overlaping(d, r, R)


def easeInOutQuad(xi, xf, t):
    """Ease trajectory between xi and xf during times t."""
    t = t - t[0]
    T = t[-1] - t[0]
    x = np.where(
        t < T / 2,
        0.5 * (2 * t / T) ** 2,
        -0.5 * ((2 * t / T - 1) * (2 * t / T - 3) - 1),
    )
    return x * (xf - xi) + xi


def traj_from_keys(Xs, Ts, t):
    """Build trajectory from keyframes.

    Xs : <1d, 2d, or 3d array> locations at key frames.
    Ts : <1d array of floats 0->1 > key frames.
    t : <1d array of floats 0->1 > all frames.
    """
    x = np.empty_like(t)
    for i in range(len(Ts) - 1):  # for each key frame interval
        inds = np.arange(  # indices of time steps for that interval
            np.where(t >= Ts[i])[0][0],
            np.where(t >= Ts[i + 1])[0][0] + 1,
        )
        ts = t[inds]
        # print(f"{Ts[i]}->{Ts[i+1]} : {ts}")
        x[inds] = easeInOutQuad(Xs[i], Xs[i + 1], ts)

    return x


def generate_activity(t,n=1, p=0.01, tau=0.01):
    dffs = np.empty((n,len(t)))
    bases = np.random.normal()
    for i in range(n):
        act = np.random.choice([0,1], size=len(t), p=[1-p, p])
        dff = np.convolve(act, np.exp(-t/tau), mode="full")[:len(t)]
        dffs[i] = dff * 100 / np.max(dff)
    return dffs
