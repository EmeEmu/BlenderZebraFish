"""Call objects and methods from lib to build Blender annimations."""

import sys
import importlib
import bpy
import numpy as np

import sys
sys.path.insert(0,'./lib')
#from LightSheet import gaussian_beam
import LightSheet as LS
import Neurons as NS
import Routines as RT
importlib.reload(LS)
importlib.reload(NS)
importlib.reload(RT)

# remove all objects from the scene
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False, confirm=False)

# remove all collections
for col in bpy.data.collections:
    bpy.data.collections.remove(col)



#LS.gaussian_beam()
#LS.light_sheet()
#LS.filter()

#NS.neurons(N=5,scale=5,origin="random")
#NS.animate(np.arange(0,120,10))


#bpy.context.scene.frame_set(40)
#bpy.context.active_object.pass_index = 100
#bpy.context.active_object.keyframe_insert(data_path="pass_index")

#RT.gcamp(startT=0,stopT=500,limit_animation=True)
#RT.lightsheet(startT=0,stopT=200,limit_animation=True)
RT.multi_lightsheet(
    startT=0, stopT=500,limit_animation=True,
    nNeurons=11, span=25
)