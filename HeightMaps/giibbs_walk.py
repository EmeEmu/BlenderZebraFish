import bpy
import bmesh

import sys
sys.path.insert(0,'./')
#from MCMC_Gibbs_HeightMaps import HeightMap
import numpy as np


# remove all objects from the scene
bpy.ops.object.select_all(action="SELECT")
bpy.ops.object.delete(use_global=False, confirm=False)

# remove all collections
for col in bpy.data.collections:
    bpy.data.collections.remove(col)
    
# remove all materials
for material in bpy.data.materials:
    material.user_clear()
    bpy.data.materials.remove(material)
    
    
def create_collection():
    """Create a new collection for Light Sheet related objects."""
    collections = [col[0] for col in bpy.data.collections.items()]
    if "Neuron_collection" not in collections:
        neuron_col = bpy.data.collections.new("Pos_collection")
        bpy.context.scene.collection.children.link(neuron_col)
    else:
        neuron_col = bpy.data.collections["Pos_collection"]
    return neuron_col

    

# import materials
bpy.ops.wm.append(
    filename="mountain", 
    directory="materials.blend\\Material\\"
)
bpy.ops.wm.append(
    filename="path", 
    directory="materials.blend\\Material\\"
)

# lighting and camera
bpy.ops.object.light_add(
    type='POINT', 
    radius=1, 
    align='WORLD', 
    location=(12, 0, 10), 
    scale=(1, 1, 1)
)
bpy.data.objects['Point'].data.energy = 10000

bpy.ops.object.camera_add(
    location=(0, -11, 8), 
    rotation=(0.943728, 5.56401e-07, -0.00678823)
)
bpy.data.objects['Camera'].data.lens = 25



# create mountain range
N = 200 # nb subdivision
planesize = 2 # size of the plane
scale = 10
imagepath = "images/Wiki_CopyLeft_blured.png"

bpy.ops.mesh.primitive_grid_add(
    x_subdivisions=N, 
    y_subdivisions=N, 
    size=planesize, 
    enter_editmode=False, 
    align='WORLD', 
    location=(0, 0, 0), 
    scale=(1, 1, 1)
)
bpy.ops.object.shade_smooth()
target = bpy.context.active_object
heightTex = bpy.data.textures.new('HeightTexture', type = 'IMAGE')
bpy.data.images.load(imagepath, check_existing=True)
heightTex.image = bpy.data.images[imagepath.split("/")[-1]]
heightTex.extension = 'CLIP'
mod = target.modifiers.new("mountain",'DISPLACE')
mod.texture = heightTex
mod.mid_level = 0
mod.strength = 0.4
bpy.context.object.scale = (scale, scale, scale)
bpy.context.object.data.materials.append(bpy.data.materials["mountain"])

# get mesh vertex coordinates
obj = bpy.context.active_object
depsgraph = bpy.context.evaluated_depsgraph_get()
bm = bmesh.new()
bm.from_object( obj, depsgraph )
bm.verts.ensure_lookup_table()
verts = np.empty((len(bm.verts), 3))
for i,v in enumerate(bm.verts):
    coords = (obj.matrix_world @ v.co)[:]
    verts[i] = np.array([*coords])
bm.free()

def find_closets(refs, exps):
    dist = np.linalg.norm(refs[:, None, :] - exps[None, :, :], axis=-1)
    return np.argmin(dist, axis=0)


# create path
radius = 0.1
#xyz = np.loadtxt("paths/WCLb_RW_1.txt")
xyz = np.loadtxt("paths/WCLb_G_1.txt")[:100]
imagesize=257

xyz[:,[0,1]] = xyz[:,[1,0]]

xyz[:,0] = xyz[:,0] * planesize*scale/imagesize - scale
xyz[:,1] = xyz[:,1] * planesize*scale/imagesize - scale
xyz[:,1] = -xyz[:,1]
xyz[:,2] = verts[
        find_closets( verts[:,[0,1]] , xyz[:,[0,1]] ), 2
        ] + radius
"""
# make a new curve
crv = bpy.data.curves.new('crv', 'CURVE')
crv.dimensions = '3D'

# make a new spline in that curve
spline = crv.splines.new(type='NURBS')

# a spline point for each point
spline.points.add(len(xyz)-1) # theres already one point by default

# assign the point coordinates to the spline points
for p, new_co in zip(spline.points, xyz):
    p.co = (list(new_co) + [1.0]) # (add nurbs weight)

# make a new object with the curve
obj = bpy.data.objects.new('path', crv)
bpy.context.scene.collection.objects.link(obj)

bpy.data.objects['path'].data.bevel_mode = 'ROUND'
bpy.data.objects['path'].data.bevel_depth = 0.03
bpy.data.objects['path'].data.use_fill_caps = True

bpy.context.scene.frame_set(1)
bpy.data.objects['path'].data.bevel_factor_end = 0
bpy.data.objects['path'].data.keyframe_insert("bevel_factor_end")
bpy.context.scene.frame_set(120)
bpy.data.objects['path'].data.bevel_factor_end = 1
bpy.data.objects['path'].data.keyframe_insert("bevel_factor_end")

bpy.data.objects['path'].data.materials.append(bpy.data.materials["path"])
"""


# create points
pos_col = create_collection()
for i in range(len(xyz)):
    bpy.ops.mesh.primitive_uv_sphere_add(
                radius=radius,
                enter_editmode=False,
                align="WORLD",
                location=xyz[i],
                scale=(1, 1, 1),
            )
    bpy.ops.object.shade_smooth()
    obj = bpy.context.active_object
    obj.name = f"pos_{i}"
    bpy.ops.collection.objects_remove_all()  # remove obj from other collections
    pos_col.objects.link(obj)  # assign obj to collection
    obj.data.materials.append(bpy.data.materials["path"])

for i in range(120):
        bpy.context.scene.frame_set(T[i])
        bpy.data.objects["neuron_0"].pass_index = dff[i]
        bpy.data.objects["neuron_0"].keyframe_insert(data_path="pass_index")
