"""Collection of Markov chain MonteCarlo alorithms for 2D maps samplings."""
import sys

import imageio
import matplotlib.pyplot as plt
import numpy as np


class MCMC(object):
    """Template class for Markov chain MonteCarlo algorithms."""

    name = "Generic MCMC class"

    def __init__(self, energy_map, temperature=1):
        """Initiate probability distribution.

        Parameters :
            :energy_map: <2D array or string> the 2D energy map of the system.
                        if is an array : uses array as energy map.
                        if is a string : load map from path.
            :temperature: <number> the boltzmann temperature.

        Attributes :
            :energy_map: <2D array or string> the 2D energy map of the system.
            :proba: <2D array> the 2D probability map of the system.
            :x: <1D array> x componant of sampling.
            :y: <1D array> y componant of sampling.

            :_shape: <tuple of 2 ints> the shape of the system.
                            i.e. ( len(_X), len(_Y) ).
            :_X: <1D array> the x states of the system.
            :_Y: <1D array> the y states of the system.
        """
        # Loading energy map
        if type(energy_map) == str:
            print("Loading energy map from file.")
            energy_map = np.array(imageio.imread(energy_map, as_gray=True))
        elif type(energy_map) == np.ndarray:
            print("Using array as energy map.")
        else:
            raise TypeError(
                    f":energy_map: is of type {type(energy_map)}. "
                    "Should be array or string."
                    )

        # Checking dimentionality
        self._shape = np.shape(energy_map)
        if len(self._shape) != 2:
            raise TypeError(f":energy_map: is of shape {self._shape}. Should be 2D.")
        self._X = np.arange(self._shape[0])
        self._Y = np.arange(self._shape[1])

        # Create Probability map
        self.energy_map = energy_map / np.max(energy_map)
        self.proba = np.exp(-self.energy_map/temperature)

        # Initiate samples
        self.x, self.y = None, None

    def sample(self, N=100, burnin=10, x0=None, y0=None) -> (np.ndarray, np.ndarray):
        """Perform the sampling.

        :N: <int> number of samples steps after thermalisation.
        :burnin: <int> number of burn in sample state before thermalisation.
        :x0: <int> ind of initial state (i.e. _X[x0] will be initial x state).
                    if None : uses random_point.
        :y0: <int> ind of initial state (i.e. _Y[y0] will be initial y state).
                    if None : uses random_point.
        """
        raise NotImplementedError(
                "The sample function is not implemented in the template class."
                )

    def _sample_step(self, xi, yi) -> (int, int):
        """Perform the a sampling step.

        :xi: <int> ind of before state.
        :yi: <int> ind of before state.
        """
        raise NotImplementedError(
                "The _sample_step is not implemented in the template class."
                )

    def show(self, traj=False):
        """Display energy map and samples if they exist.

       :traj: <bool> plot lines between samples.
        """
        fig, ax = plt.subplots(ncols=1, figsize=(10, 10))
        ax.imshow(self.energy_map, cmap="cividis", origin="lower")
        if (self.x is not None) and (self.y is not None):
            ax.scatter(self.y, self.x, color="red", s=1)
            if traj:
                ax.plot(self.y, self.x, color="red", linewidth=0.5, alpha=0.75)
        fig.show()

    def random_point(self, n=1):
        """Return random initial position(s).

        :n: <int> number of points to generate.

        return
            :x:,:y: the x and y positions
        """
        x = np.random.randint(0, self._shape[0], size=n)
        y = np.random.randint(0, self._shape[1], size=n)
        return x, y


class RandomWalk(MCMC):
    """Most basic MCMC, implementing a simple random walk."""

    def sample(self, N=100, burnin=10, x0=None, y0=None, pturn=0.5, dxy=10):
        """Perform the sampling.

        :N: <int> number of samples steps after thermalisation.
        :burnin: <int> number of burn in sample state before thermalisation.
        :x0: <int> ind of initial state (i.e. _X[x0] will be initial x state).
                    if None : uses random_point.
        :y0: <int> ind of initial state (i.e. _Y[y0] will be initial y state).
                    if None : uses random_point.
        """
        if x0 is None:
            x0, y0 = self.random_point(n=1)

        self.x, self.y = np.empty(N), np.empty(N)

        self._angle = 0
        xprev, yprev = x0, y0
        for t in range(burnin):
            xprev, yprev = self._sample_step(xprev, yprev, pturn, dxy)

        self.x[0], self.y[0] = xprev, yprev
        for t in range(1, N):
            self.x[t], self.y[t] = self._sample_step(
                    self.x[t-1],
                    self.y[t-1],
                    pturn,
                    dxy
                    )

    def _sample_step(self, xi, yi, pturn, dxy):
        """Perform the a sampling step.

        :xi: <int> ind of before state.
        :yi: <int> ind of before state.
        """
        xj, yj = self._shape
        while not (0 <= xj < self._shape[0]) or not (0 <= yj < self._shape[1]):
            turn = np.random.choice([True, False], size=1, p=[pturn, 1-pturn])
            if turn:
                self._angle = np.random.uniform(low=0, high=2*np.pi, size=1)
            xj = (xi + dxy * np.cos(self._angle)).astype(int)
            yj = (yi + dxy * np.sin(self._angle)).astype(int)
        return xj, yj


class Gibbs(MCMC):
    """Implementing Gibbs Sampling."""

    def sample(self, N=100, burnin=10, skip=1, x0=None, y0=None):
        """Perform the sampling.

        :N: <int> number of samples steps after thermalisation.
        :burnin: <int> number of burn in sample state before thermalisation.
        :x0: <int> ind of initial state (i.e. _X[x0] will be initial x state).
                    if None : uses random_point.
        :y0: <int> ind of initial state (i.e. _Y[y0] will be initial y state).
                    if None : uses random_point.
        """
        if x0 is None:
            x0, y0 = self.random_point(n=1)
            x0, y0 = x0[0], y0[0]
        else:
            x0, y0 = int(x0), int(y0)

        self.x, self.y = (
                np.empty(N, dtype=int), 
                np.empty(N, dtype=int)
                )

        self._angle = 0
        xprev, yprev = x0, y0
        for t in range(burnin):
            xprev, yprev = self._sample_step(xprev, yprev)

        self.x[0], self.y[0] = xprev, yprev
        for t in range(1, N):
            for _ in range(skip):
                xprev, yprev = self._sample_step(xprev, yprev)
            self.x[t], self.y[t] = xprev, yprev

    def _sample_step(self, xi, yi):
        """Perform the a sampling step.

        :xi: <int> ind of before state.
        :yi: <int> ind of before state.
        """
        xj = self._sample_Px(yi)
        yj = self._sample_Py(xj)
        return xj, yj

    def _sample_Py(self, x):
        """Sample P(y|x) i.e. pick y out of P(y|x)."""
        Py = self.proba[x, :]
        Py /= np.sum(Py)
        return np.random.choice(self._Y, size=1, p=Py)[0]

    def _sample_Px(self, y):
        """Sample P(x|y) i.e. pick x out of P(x|y)."""
        Px = self.proba[:, y]
        Px /= np.sum(Px)
        return np.random.choice(self._X, size=1, p=Px)[0]


class GibbsLocal(Gibbs):
    """Implementing Gibbs Sampling with single state evolution."""

    def _sample_step(self, xi, yi):
        """Perform the a sampling step.

        :xi: <int> ind of before state.
        :yi: <int> ind of before state.
        """
        xj = self._sample_Px(yi, xi)
        yj = self._sample_Py(xj, yi)
        return xj, yj

    def _sample_Py(self, x, y):
        """Sample P(y|x) i.e. pick y out of P(y|x)."""
        mmin, mmax = max(0, y-1), min(y+2, self._shape[1])
        # print(y, self._Y[mmin:mmax])
        Py = self.proba[x, mmin:mmax]
        Py /= np.sum(Py)
        return np.random.choice(self._Y[mmin:mmax], size=1, p=Py)[0]

    def _sample_Px(self, y, x):
        """Sample P(x|y) i.e. pick x out of P(x|y)."""
        mmin, mmax = max(0, x-1), min(x+2, self._shape[0])
        Px = self.proba[mmin:mmax, y]
        Px /= np.sum(Px)
        return np.random.choice(self._X[mmin:mmax], size=1, p=Px)[0]
