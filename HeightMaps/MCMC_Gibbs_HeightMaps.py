"""Perform Gibbs sampling on 2D height maps to show off for outreach."""
import imageio
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm


class HeightMap(object):
    """Handle height maps and Gibbs sampling"""

    def __init__(self, height_map_path, energy_scale=50):
        """Defines the height maps.

        :height_map_path: <string> path to te height map image.
        :energy_scale: <number> scaling of height map (0 -> :energy_scale:).

        :hm: <2d array> height map.
        :shape: <tuple of 2 ints> shape of :hm: .

        :_xs: <1d array of ints> values of x (0 -> shape[0]).
        :_ys: <1d array of ints> values of y (0 -> shape[1]).
        :_P: <2d array> Boltzmann proba from energy map :hm:.
                        ( !! non normalised !! ).
        """
        self.hm = np.array(imageio.imread(height_map_path, as_gray=True))
        self.hm = self.hm / np.max(self.hm) * energy_scale
        self.shape = self.hm.shape

        self._xs = np.arange(self.shape[0])
        self._ys = np.arange(self.shape[1])
        self._P = np.exp(-self.hm)

    def show(self, xs=None, ys=None):
        """Show height map and trajectory.

        :xs: <1d array> x positions of walker.
        :ys: <1d array> y positions of walker.
        """
        fig, ax = plt.subplots(ncols=1, figsize=(10, 10))
        ax.imshow(self.hm, cmap="cividis", origin="lower")
        if (xs is not None) and (ys is not None):
            ax.plot(ys, xs, color="red", linewidth=0.5, alpha=0.75)
            ax.scatter(ys, xs, color="red", s=1)
        plt.show()

    def _random_point(self, n=1):
        """Return random initial position(s).

        :n: <int> number of points to generate.

        return
            :x:,:y: the x and y positions
        """
        x = np.random.randint(0, self.shape[0], size=n)
        y = np.random.randint(0, self.shape[1], size=n)
        return x, y

    def _sample_Py(self, x):
        """Sample P(y|x) i.e. pick y out of P(y|x)."""
        Py = self._P[x, :]
        Py /= np.sum(Py)
        return np.random.choice(self._ys, size=1, p=Py)

    def _sample_Px(self, y):
        """Sample P(x|y) i.e. pick x out of P(x|y)."""
        Px = self._P[:, y]
        Px /= np.sum(Px)
        return np.random.choice(self._xs, size=1, p=Px)

    def _gibbs_step(self, x0, y0):
        """Perform sampling step from (x0,y0).

        :x0: <int> initial x position.
        :y0: <int> initial y position.

        return
            :x:,:y: sampled position.
        """
        x = self._sample_Px(y0)[0]
        y = self._sample_Py(x)[0]
        return x, y

    def gibbs(self, n=10, x0=0, y0=0):
        """Perform Gibbs sampling.

        :n: <int> number of gibbs steps.
        :x0: <int> initial x position.
        :y0: <int> initial y position.

        return
            :X:,:Y: sequences of x and y sampled positions.
        """
        X = np.empty(n, dtype=int)
        Y = np.empty(n, dtype=int)
        X[0] = x0
        Y[0] = y0
        for i in tqdm(range(1, n)):
            X[i], Y[i] = self._gibbs_step(X[i - 1], Y[i - 1])
        return X, Y

    def random_walk(self, n=10, x0=0, y0=0, dxy=5, pturn=0.1):
        X = np.empty(n, dtype=int)
        Y = np.empty(n, dtype=int)
        X[0] = x0
        Y[0] = y0
        angle = 0
        turns = np.random.choice([True, False], size=n, p=[pturn, 1 - pturn])
        for i in tqdm(range(1, n)):
            if turns[i]:
                angle = np.random.random(size=1) * 2 * np.pi
            X[i] = X[i - 1] + dxy * np.cos(angle)
            Y[i] = Y[i - 1] + dxy * np.sin(angle)
        return X, Y
